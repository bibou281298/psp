[RMXP] Pokemon Script Project

Cr�� par :
Krosk

[PSP] Version 1.0 - Alpha - 28/01/2020

Contributeurs internes :
Responsable :
Damien Linux

Scripts :
Akise Dogma
Damien Linux
Danny Phenton
FL0RENT_
Katsuki
Lizen
Metaiko

Events :
Danny Phenton
Eurons

Corrections diverses :
Katsuki
Metaiko
Symbioss
yyyyj
ralandel

Tests :
Akise Dogma
Damien Linux
Metaiko
PixelCraft
Symbioss

Contributeurs externes :
Scripts :
Aerun
Krosk
Leikt
Louro
maror
Nuri Yuri
PokeProject
Therand
Youspin

Tests :
BladeRED

[PSP] versions ant�rieures

Responsables :
Danny Phenton
FL0RENT_
Krosk
Nuri Yuri
Palbolsky
Rhenaud The Lukark
Zohran

Scripts :
Brendan75
Danny Phenton
Drakhaine
FL0RENT_
Krosk
MoussPSP
N�va
Nuri Yuri
Palbolsky
Rhenaud the Lukark
Solfay1
Sphinx
Tonyryu
Youspin
yyyyj

Events :
Danny Phenton
Slash
Symbioss

Mapping :
Slash

Ressources :
Bibiantonio
Danny Phenton
Guille-Kun
Kezal
Link
Mortenkein

Graphismes :
Alex
Bazaro
Conyjams
DatLopunnyTho
Falaia of the Smogon S/M sprite project
fishbowlsoul90
Jan
KaiserYoshi
kaji atsu
Koyo
Leparagon
Lord-Myre
LuigiPlayer
Mortenkein
N-kin
Noscium
Pikafan2000
princess-phoenix
sansonic
Smeargletail
The cynical poet
Zumi

Animations d�attaques :
Bibiantonio
Ghioa
Isomir
KLNOTHINCOMIN
Leparagon
Metaiko

BDD :
DJStarmix
FL0RENT_
Speed
Symbioss
yyyyj

Tests :
BladeRED

Carte dresseur :
Louro

Mod�le Pok�dex :
Shaolan

[PSP 0.4] Autres contributions :
Ace
BoOmxBig
Denis bros
Dymew
Empire1601
Endoh
Ergg
ITA
Jordan
L�n
Light-PA
Louro
Midnitez-REMIX
Miniyas
Mister-k
NT
New Titeuf
Pichou
RMXPStudio
Spiky
TyranitarDark
Warpras
Wesker
Zelda

Sites de r�cup�ration des informations :
Bulbapedia
Pokepedia

Licences (tous droits r�serv�s) :
Creature Inc.
Enterbrain
Game Freak
Nintendo
The Pokemon Company