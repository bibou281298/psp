Sujet de la news : PSP 1.0 - Alpha 0.8.7
Date de la news : 19/01/2020

Nouvelle release disponible qui inclut :
- La possibilité d'attribuer des objets aux pokémon sauvages automatiquement
- L'utilisation des chaussures de sport
- Des rectifications sur la démo
- Réglages de la hauteur des sprites en fin de combat
- Rectification d'un bug de l'IA des Switch