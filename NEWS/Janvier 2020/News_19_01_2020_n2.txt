Sujet de la news : Bug important sur les Alpha 0.8.6 et 0.8.7
Date de la news : 19/01/2020

Veuillez ne pas garder les sauvegardes sur vos projets basés sur PSP 1.0 - Alpha 0.8.6 et 0.8.7 !
Il y a un décalage sur les sauvegardes des anciennes versions au niveau des talents dû à la fin de la BDS.
Un patch sera apporté sur PSP 1.0 - Alpha 0.8.8 mais provoquera un décalage sur les versions basés sur PSP 1.0 - Alpha 0.8.6 et 0.8.7.

Des scripts de secours vous seront fourni si jamais afin de régler tout décalage. Mais par prudence veuillez ne plus baser vos
sauvegardes sur ces versions si ce n'est pas déjà fait.