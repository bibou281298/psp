PSP 1.0 - Alpha 0.8.10 - Distributu� par Damien Linux (merci de lire le fichiers Cr�dits.txt)
Rappel : Il s'agit d'une version Alpha, le d�veloppement de PSP 1.0 est actuellement en cours.
Il se peut que des bugs soient pr�sents. Il est conseill� de faire une copie de votre projet pour
�viter tout probl�me. De plus des mises � jour r�guli�res sont faites. Pour avoir un projet stable,
je vous conseille de migrer sur la version 1.0 une fois qu'elle sera sortie officiellement.

N'h�sitez pas � me contact : par message priv� sur Pok�mon Workshop (Damien Linux), sur discord : Damien Linux#3601, par mail : damien.3199@gmail.com

Vocabulaire :
release = Nouvelle mise � jour d'un logiciel

Cette release est pr�sente afin de vous partager l'�volution successive de Pok�mon Script Project.
IMPORTANT : Si vous avez t�l�charg� la release via le launcher, veuillez faire une copie de ce projet ou
le d�placer afin d'�viter que lorsque vous cliquerez la prochaine fois sur le launcher, d'�craser la release pr�c�dente
par celle courante.

Les dossiers (hors-projet) :
NEWS : Dossier contenant tous les news des �volutions de PSP.
RELEASES : Dossier contenant toutes les d�marches d'installations manuelles des scripts en cours. Une release = une archive

Informations sur l'archivage des News :
Les News se retrouvent � la racine du dossier "NEWS". Cependant pour les news des mois pr�c�dents, elles sont archiv�s dans
des dossiers pour pouvoir s'y retrouver, exemple : "Janvier 2020". Si vous t�l�chargez les mises � jour par le launcher, vous
aurez peut �tre � la fois le dossier "Janvier 2020" par exemple mais son contenu se trouvera �galement en-dehors du dossier.
Pensez � supprimer les fichiers se trouvant en-dehors de leur dossier pour vous y retrouver ;)

Historique des releases :
PSP 1.0 - Alpha 0.8.6 - 18/01/2020
PSP 1.0 - Alpha 0.8.7 - 19/01/2020
PSP 1.0 - Alpha 0.8.8 - 20/01/2020
PSP 1.0 - Alpha 0.8.9 - 28/01/2020
PSP 1.0 - Alpha 0.8.10 - 01/02/2020