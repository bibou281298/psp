APRES RESTRUCT V2

d�pendance : aucune

Dans Pokemon_Attacks_Duration_Annexe l.54 :
Remplacez :
@user.skill_effect(:ligotement, turn)
Par :
@target.skill_effect(:ligotement, turn)

Dans Pokemon_Battle_Pre_Post_Rounds l.212
Remplacez :
execution(effect, "pre_post_soin", actor)
Par :
execution(effect, "pre_post_soin", actor_list, enemy_list)

l.221 :
Remplacez :
execution(effect, "pre_post_enemy_piege", actor, enemy)
Par :
enemy.effect_list.each do |effect|
        execution(effect, "pre_post_enemy_piege", actor_list, enemy_list)
      end
	  
l.245 :
Remplacez :
execution(effect, "pre_post_secondaire", actor)
Par :
execution(effect, "pre_post_secondaire", actor_list, enemy_list)

l.208 :
En-dessous de :
# --------- -------------- --------------------
      #    Programmation des attaques et des capa
      # --------- -------------- --------------------
Ajoutez :
actor_list = [actor, actor_status, actor_sprite]
      enemy_list = [enemy, enemy_status, enemy_sprite]
	  
Dans Pokemon_Attacks_Pre_Post_Rounds l.69 :
Remplacez :
#------------------------------------------------------------  
    # Soin p�riodique
    #------------------------------------------------------------
    def vampigraine_pre_post_soin(actor)
      malus = actor.max_hp / 8
      draw_text("L'�nergie de #{actor.given_name}","est drain�e !")
      heal(actor, actor_sprite, actor_status, -malus)
      heal(enemy, enemy_sprite, enemy_status, malus)
      wait(40)
    end

    def racines_pre_post_soin(actor)
      bonus = actor.max_hp / 16
      draw_text(actor.given_name + " puise", "de l'�nergie dans la terre.")
      heal(actor, actor_sprite, actor_status, bonus)
      wait(40)
    end

    def anneau_hydro_pre_post_soin(actor)
      bonus = actor.max_hp / 16
      draw_text(actor.given_name + " se soigne", "gr�ce � ANNEAU HYDRO!")
      heal(actor, actor_sprite, actor_status, bonus)
      wait(40)
    end
    
    #------------------------------------------------------------  
    # Pi�ges de l'ennemi s'activant sur l'acteur
    #------------------------------------------------------------ 
    def ligotement_pre_post_enemy_piege(actor)
      damage = actor.max_hp / 16
      draw_text(actor.given_name, "est pi�g� !")
      self_damage(actor, actor_sprite, actor_status, damage)
      wait(40)
    end
    
    #------------------------------------------------------------  
    # Effets secondaires
    #------------------------------------------------------------ 
    def cauchemard_pre_post_secondaire(actor)
      if actor.asleep?
        damage = actor.max_hp / 4
        draw_text(actor.given_name + " fait", "un chauchemar !")
        heal(actor, actor_sprite, actor_status, -damage)
        wait(20)
      else
        index = actor.effect_list.index(:cauchemard)
        actor.effect.delete_at(index)
      end
    end

    def malediction_pre_post_secondaire(actor)
      damage = actor.max_hp / 4
      draw_text(actor.given_name + " est", "maudit!")
      heal(actor, actor_sprite, actor_status, -damage)
      wait(20)
    end

    def brouhaha_pre_post_secondaire(actor)
      if actor.asleep?
        actor.cure
        draw_text(actor.given_name + " se r�veille", "� cause du brouhaha !")
        wait(40)
      end
      if actor.frozen? # Fin de l'effet
        index = actor.effect_list.index(:brouhaha)
        actor.effect.delete_at(index)
      end
    end

    def provoc_pre_post_secondaire(actor)
      index = actor.effect_list.index(:provoc)
      actor.skills_set.each do |skill|
        if skill.power == 0 and actor.effect[index][1] > 0
          draw_text(skill.name + " est bloqu� !")
          skill.disable
          wait(40)
        elsif actor.effect[index][1] == 0
          draw_text(skill.name + " est r�tablit.")
          skill.enable
          wait(40)
        end
      end
    end

    def baillement_pre_post_secondaire(actor)
      index = actor.effect_list.index(:baillement)
      if actor.status == 0 and actor.effect[index][1] == 0
        status_check(actor, 4)
        actor_status.refresh
      end
    end	
Par :
#------------------------------------------------------------  
    # Soin p�riodique
    # actor[0] : actor / actor[1] : actor_status / actor[2]  actor_sprite
    # enemy[0] : enemy / enemy[1] : enemy_status / enemy[2]  enemy_sprite
    #------------------------------------------------------------
    def vampigraine_pre_post_soin(actor, enemy)
      malus = actor[0].max_hp / 8
      draw_text("L'�nergie de #{actor[0].given_name}","est drain�e !")
      heal(actor[0], actor[2], actor[1], -malus)
      heal(enemy[0], enemy[2], enemy[1], malus)
      wait(40)
    end

    def racines_pre_post_soin(actor, enemy)
      bonus = actor[0].max_hp / 16
      draw_text(actor[0].given_name + " puise", "de l'�nergie dans la terre.")
      heal(actor[0], actor[2], actor[1], bonus)
      wait(40)
    end

    def anneau_hydro_pre_post_soin(actor, enemy)
      bonus = actor[0].max_hp / 16
      draw_text(actor[0].given_name + " se soigne", "gr�ce � ANNEAU HYDRO!")
      heal(actor[0], actor[2], actor[1], bonus)
      wait(40)
    end
    
    #------------------------------------------------------------  
    # Pi�ges de l'ennemi s'activant sur l'acteur
    # actor[0] : actor / actor[1] : actor_status / actor[2]  actor_sprite
    # enemy[0] : enemy / enemy[1] : enemy_status / enemy[2]  enemy_sprite
    #------------------------------------------------------------ 
    def ligotement_pre_post_enemy_piege(actor, enemy)
      damage = enemy[0].max_hp / 16
      draw_text(enemy[0].given_name, "est pi�g� !")
      self_damage(enemy[0], enemy[2], enemy[1], damage)
      wait(40)
    end
    
    #------------------------------------------------------------  
    # Effets secondaires
    # actor[0] : actor / actor[1] : actor_status / actor[2]  actor_sprite
    # enemy[0] : enemy / enemy[1] : enemy_status / enemy[2]  enemy_sprite 
    #------------------------------------------------------------ 
    def cauchemard_pre_post_secondaire(actor, enemy)
      if actor[0].asleep?
        damage = actor[0].max_hp / 4
        draw_text(actor[0].given_name + " fait", "un chauchemar !")
        heal(actor[0], actor[2], actor[1], -damage)
        wait(20)
      else
        index = actor[0].effect_list.index(:cauchemard)
        actor[0].effect.delete_at(index)
      end
    end

    def malediction_pre_post_secondaire(actor, enemy)
      damage = actor[0].max_hp / 4
      draw_text(actor[0].given_name + " est", "maudit!")
      heal(actor[0], actor[2], actor[1], -damage)
      wait(20)
    end

    def brouhaha_pre_post_secondaire(actor, enemy)
      if actor[0].asleep?
        actor[0].cure
        draw_text(actor[0].given_name + " se r�veille", "� cause du brouhaha !")
        wait(40)
      end
      if actor[0].frozen? # Fin de l'effet
        index = actor[0].effect_list.index(:brouhaha)
        actor[0].effect.delete_at(index)
      end
    end

    def provoc_pre_post_secondaire(actor, enemy)
      index = actor[0].effect_list.index(:provoc)
      actor[0].skills_set.each do |skill|
        if skill.power == 0 and actor[0].effect[index][1] > 0
          draw_text(skill.name + " est bloqu� !")
          skill.disable
          wait(40)
        elsif actor[0].effect[index][1] == 0
          draw_text(skill.name + " est r�tablit.")
          skill.enable
          wait(40)
        end
      end
    end

    def baillement_pre_post_secondaire(actor, enemy)
      index = actor[0].effect_list.index(:baillement)
      if actor[0].status == 0 and actor[0].effect[index][1] == 0
        status_check(actor[0], 4)
        actor[1].refresh
      end
    end
    
    #------------------------------------------------------------  
    # Nettoyage des effets
    # actor[0] : actor / actor[1] : actor_status / actor[2]  actor_sprite
    # enemy[0] : enemy / enemy[1] : enemy_status / enemy[2]  enemy_sprite 
    #------------------------------------------------------------ 
    def mur_lumiere_pre_post_compteur(actor, compteur)
      if compteur == 0
        draw_text("L'effet de MUR LUMIERE", "prend fin.")
        wait(40)
      end
    end

    def brume_pre_post_compteur(actor, compteur)
      if compteur == 0
        draw_text("La brume se dissipe.")
        wait(40)
      end
    end

    def protection_pre_post_compteur(actor, compteur)
      if compteur == 0
        draw_text("L'effet de PROTECTION", "prend fin.")
        wait(40)
      end
    end

    def requiem_pre_post_compteur(actor, compteur)
      index = actor[0].effect_list.index(:requiem)
      number = actor[0].effect[index][1]
      if number > 0
        if number > 1
          string = "#{number.to_s} tours"
        elsif number == 1
          string = "#{number.to_s} tour"
        end
        draw_text("Plus que #{string}", "pour #{actor[0].given_name}...")
        wait(40)
      else
        draw_text("#{actor[0].given_name} est", "K.O. par REQUIEM !")
        damage = actor[0].hp
        heal(actor[0], actor[2], actor[1], -damage)
        wait(40)
      end
    end

    def rune_protect_pre_post_compteur(actor, compteur)
      if compteur == 0
        draw_text("L'effet de RUNE PROTECT", "prend fin.")
        wait(40)
      end
    end

    def embargo_pre_post_compteur(actor, compteur)
      if compteur == 0
        draw_text("L'effet d'EMBARGO","prend fin.") 
        wait(40)
      end
    end

    def anti_soin_pre_post_compteur(actor, compteur)
      if compteur == 0
        draw_text("L'effet d'ANTI-SOIN","prend fin.") 
        wait(40)
      end
    end

    def air_veinard_pre_post_compteur(actor, compteur)
      if compteur == 0
        draw_text("L'effet d'AIR VEINARD","prend fin.") 
        wait(40)
      end
    end