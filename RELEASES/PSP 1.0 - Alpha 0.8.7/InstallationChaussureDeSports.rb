APRES RESTRUCT V2

d�pendance : aucune

Dans Game_Player_SG l. 100 :
En-dessous de :
# Lorsque le bouton C est enfonc�
      if Input.trigger?(Input::C)
        # Jugement d'activation d'�v�nement de la m�me position et du m�me front
        check_event_trigger_here([0])
        check_event_trigger_there([0,1,2])
        # Impl�mentation Surf
        if $game_map.passable?(front_tile[0],front_tile[1], 10 - $game_player.direction) and 
            $game_player.terrain_tag != 7 and $game_map.terrain_tag(front_tile[0], front_tile[1]) == 7 and
            not $game_system.map_interpreter.running?
          $game_temp.common_event_id = POKEMON_S::Skill_Info.map_use(POKEMON_S::Skill_Info.id("SURF"))
        end
      end
	  
Ajoutez :
# Impl�mentation chaussures de sport
      # Utilise l'interrupteur n�20 par d�faut 
      # Par d�faut il faut appuyer sur la touche Shift pour utiliser les chaussures
      if Input.press?(Input::A) and $game_switches[CHAUSSURE_DE_SPORT] and
         not $game_switches[EN_BICYCLETTE] and $game_player.terrain_tag != 7 and
         $game_map.terrain_tag(front_tile[0], front_tile[1]) != 7 and
         not $game_system.map_interpreter.running? 
        if (!@character_name.include?("_sport"))
          @name = @character_name
        end
        if Input.press?(Input::UP) or Input.press?(Input::DOWN) or
            Input.press?(Input::RIGHT) or Input.press?(Input::LEFT)            
          if $game_map.passable?(front_tile[0],front_tile[1], 10 - $game_player.direction)
            $game_player.set_map_character("#{@name}_sport", $game_player.direction)
            @move_speed = 5
          else         
            $game_player.set_map_character(@name, $game_player.direction)  
            @move_speed = 4
          end         
        else
          $game_player.set_map_character(@name, $game_player.direction)  
          @move_speed = 4
        end               
      else
        if @character_name.include?("_sport")
          $game_player.set_map_character(@name, $game_player.direction) 
          @move_speed = 4
        end       
      end
	  
Dans Gestion_Switches_Variables l. 20 :
En-dessous de :
INTERDICTION_FUITE = 37 # Interdiction de fuire
INTERDICTION_CAPTURE = 36 # Interdiction de capturer un pok�mon

Ajoutez :
CHAUSSURE_DE_SPORT = 11 # Utilisation des chassures de sports
EN_BICYCLETTE = 51 # Lorsque l'utilisateur utilise sa bicyclette