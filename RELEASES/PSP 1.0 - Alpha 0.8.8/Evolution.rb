APRES RESTRUCT V2

dépendance : aucune

Proposé par Symbioss, de FL0RENT_
Dans Pokemon_Evolve :
Remplacez :
def evolved_sprite_generation
      ida = sprintf("%03d", @evolve_id)
      form = sprintf("_%d", @pokemon.form)
      if @pokemon.gender == 1 or @pokemon.gender == 0
        string = "Front_Male/#{ida}#{form}.png"
        if not($picture_data["Graphics/Battlers/#{string}"])
          string.sub!(form, "")
        end
      elsif @pokemon.gender == 2
        string = "Front_Female/#{ida}#{form}.png"
        if not($picture_data["Graphics/Battlers/#{string}"])
          string.sub!(form, "")
        end
        if not($picture_data["Graphics/Battlers/#{string}"])
          string = "Front_Male/#{ida}#{form}.png"
          if not($picture_data["Graphics/Battlers/#{string}"])
            string.sub!(form, "")
          end
        end
      end
   
      if @pokemon.shiny
        string2 = "Shiny_#{string}"
        #if FileTest.exist?("Graphics/Battlers/" + string2)
        if $picture_data["Graphics/Battlers/#{string2}"]
          string = string2
        end
      end
   
      return string
    end

Par :
def evolved_sprite_generation
      ida = sprintf("%03d", @evolve_id)
      form = sprintf("_%02d", @pokemon.form)
      if @pokemon.gender == 1 or @pokemon.gender == 0
        string = "Front_Male/#{ida}#{form}.png"
        if not($picture_data["Graphics/Battlers/" + string])
          string.sub!(form, "")
        end
      elsif @pokemon.gender == 2
        string = "Front_Female/#{ida}#{form}.png"
        #if not(FileTest.exist?("Graphics/Battlers/" + string))
        if not($picture_data["Graphics/Battlers/" + string])
          string.sub!(form, "")
        end
        if not($picture_data["Graphics/Battlers/" + string])
          string = "Front_Male/#{ida}#{form}.png"
          if not($picture_data["Graphics/Battlers/" + string])
            string.sub!(form, "")
          end
        end
      end
       
      if @pokemon.shiny
        string2 = "Shiny_" + string
        #if FileTest.exist?("Graphics/Battlers/" + string2)
        if $picture_data["Graphics/Battlers/" + string2]
          string = string2
        end
      end
       
      return string
    end
	