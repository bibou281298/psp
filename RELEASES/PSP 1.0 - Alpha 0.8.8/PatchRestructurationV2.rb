APRES RESTRUCT V2

d�pendance : aucune

Supprimez tous les :
if ability = 174
  actor.form = 0
      end
Il y en a 3 dans : Pokemon_Battle_Trainer, Pokemon_Battle_Core et Pokemon_Battle_Wild


Dans Pokemon_Item / Data l. 455 :
Remplacez :
return [false, pokemon.name + " conna�t d�j� " + Skill_Info.name(skill_id) + '.' ]
Par :
return [false, "#{pokemon.name} conna�t d�j� #{POKEMON_S::Skill_Info.name(skill_id)}." ]

Dans Pokemon_Battle_Methods 2 l. 446 :
Remplacez :
elsif param2 != nil
        send(method_to_call, param1) if methods.include?(method_to_call)
      elsif param1 != nil
        send(method_to_call, param2) if methods.include?(method_to_call)
	
Par :
elsif param1 != nil
        send(method_to_call, param1) if methods.include?(method_to_call)
      elsif param2 != nil
        send(method_to_call, param2) if methods.include?(method_to_call)
		
Dans Pokemon_Battle_Switch
dans les fonctions def launch_pokemon et def recall_pokemon :
Remplacez les :
@actor_sprite.y = 321
Par :
@actor_sprite.y = 301

Dans Pokemon_Methods_Interface l. 117 :
Remplacez :
les m�thodes def battler_menu et def battler_face
Par :
def battler_face(anime = true)
      ida = sprintf("%03d", id)
       
      if @egg
        string = "Eggs/#{ida}.png"
        if not( $picture_data["Graphics/Battlers/#{string}"] )
          string = "Eggs/Egg000.png"
        end
        return string
      end
       
      prefixe = anime ? "Anime/" : ""
      
      if @gender == 1 or @gender == 0
        string = "#{prefixe}stringFront_Male/#{ida}#{battler_form}.png"
      elsif @gender == 2
        string = "#{prefixe}Front_Female/#{ida}#{battler_form}.png"
        if not($picture_data["Graphics/Battlers/" + string] )
          string = "#{prefixe}Front_Male/#{ida}#{battler_form}.png"
        end
      end
             
      if @shiny
        string2 = "Shiny_#{string}"
        if $picture_data["Graphics/Battlers/#{string2}"]
          string = string2
        end
      end
       
      if not( $picture_data["Graphics/Battlers/#{string}"] )
        string.sub!(battler_form, "")
      end
       
      return string
    end
	
Recherchez tous les battler_menu et remplacez les par battler_face(false)

Dans Pokemon l. 449, supprimez :
def item_hold=(value)

Dans Pokemon_Battle_Wild l. 117, remplacez :
@enemy.item_hold=(wild_item) # modulable dans Pokemon_Objets_Sauvages

Par :
@enemy.item_hold = wild_item # modulable dans Pokemon_Objets_Sauvages

RECTIFICATION DES TALENTS :
Dans Pokemon_Custom l. 120 , supprimez :
#------------------------------------------------------------------
    # Cap.sp� par forme
    #------------------------------------------------------------------
    def ability
      return @ability + 1
    end
	
Dans Pokemon l. 45 :
Remplacez :
attr_reader :ability  

Par :
attr_accessor :ability  

Dans Pokemon_Methods_Interface :
supprimez la m�thode def ability=(id)

Dans Pokemon_Data l. 240 :
Remplacez :
34.upto($data_armors.size - 1) do |id|
      ability = $data_armors[id]
      $data_ability.push(Array.new)
      $data_ability[id-34].push(ability.name)
      $data_ability[id-34].push(ability.description)
      $data_ability[id-34].push(ability.price)
    end
	
Par :
$data_ability.push([]) # Element en 0 vide pour rester compatible avec les
                       # Versions utilisant la BDS avant PSP 1.0
    34.upto($data_armors.size - 1) do |id|
      ability = $data_armors[id]
      $data_ability.push(Array.new)
      $data_ability[id-33].push(ability.name)
      $data_ability[id-33].push(ability.description)
      $data_ability[id-33].push(ability.price)
    end
	
l. 512 :
Remplacez :
ability_list = code2 == 0 ? [$data_ability[code1-34][0]] : [$data_ability[code1-34][0], $data_ability[code2-34][0]]

Par :
ability_list = code2 == 0 ? [$data_ability[code1-33][0]] : [$data_ability[code1-33][0], $data_ability[code2-33][0]]
				
SCRIPTS DE SECOURS :
Si vous �tes bas�s sur PSP 1.0 - Alpha 0.8.6 ou 0.8.7 (ou que vous avez install� la restructuration V2) et que vous avez des sauvegardes sur cette partie,
afin que les talents ne soient pas d�cal�s sur vos sauvegardes vous devez ex�cuter un script de secours (� moins que vous pouvez supprimer les sauvegardes)

Ces scripts ne sont � ex�cuter une et une seule fois !
Dans le cas o� vous �tes bas�s sur ce qui est dit ci-dessus, vous ex�cuterez le script Patch1_0.rb
Si en plus d avoir des sauvegardes bas�s dessus vous avez des sauvegardes qui datent d avant PSP 1.0 - Apha, vous n allez pas ex�cuter le script
pr�c�dent mais : Patch0_9.rb

OUBLIS DE SYMBOLES POUR LES TALENTS
Dans Pokemon_Custom l. 52 :
Remplacez :
def item_hold=(item_id)
      @item_hold = item_id
      if name == "Deoxys"
        @form = 5 if item_id == 1
        @form = 0 if item_id != 1
      end
      if ability == 122
        @form = 0
        @form = 2 if item_id == 326
        @form = 3 if item_id == 327
        @form = 4 if item_id == 328
        @form = 5 if item_id == 329
        @form = 6 if item_id == 330
        @form = 7 if item_id == 331
        @form = 8 if item_id == 332
        @form = 9 if item_id == 333
        @form = 10 if item_id == 334
        @form = 11 if item_id == 335
        @form = 12 if item_id == 336
        @form = 13 if item_id == 337
        @form = 14 if item_id == 338
        @form = 15 if item_id == 339
        @form = 16 if item_id == 340
        @form = 17 if item_id == 341
        @form = 18 if item_id == 342
      end
      if name == "Giratina"
        @form = 0    
        @form = 1 if item_id == 343
        @ability = 46 if @form == 0
        @ability = 26 if @form == 1
      end
    end
	
Par :
def item_hold=(item_id)
      @item_hold = item_id
      if ability_symbol == :multi_type
        # 324 est la marge entre le num�ro de la forme et l'ID de l'item
        @form = item_id - 324
        @form = 0 if @form < 2 or @form > 18
      else
        case name
        when "Deoxys"
          @form = 5 if item_id == 1
          @form = 0 if item_id != 1
        when "Giratina"
          @form = item_id == 343 ? 1 : 0 
          @ability = @form == 0 ? 46 : 26
        end
      end
    end

l. 72 : 
Remplacez :
#type formes
    def type1
      if ability == 122      
        return 1 if @form == 0
        return 2 if @form == 2
        return 3 if @form == 3
        return 4 if @form == 4
        return 5 if @form == 5
        return 6 if @form == 6
        return 7 if @form == 7
        return 8 if @form == 8
        return 9 if @form == 9
        return 10 if @form == 10
        return 11 if @form == 11
        return 12 if @form == 12
        return 13 if @form == 13
        return 14 if @form == 14
        return 15 if @form == 15
        return 16 if @form == 16
        return 17 if @form == 17
        return 18 if @form == 18
      elsif ability == 59
        return 1 if @form == 0
        return 2 if @form == 2
        return 3 if @form == 3
        return 6 if @form == 6
      else
        @type1
      end
    end
	
Par :
#type formes
    def type1
      if ability_symbol == :multi_type or ability_symbol == :meteo
        type = @form > 1 ? @form : 0
      else
        type = @type1
      end
      return type
    end
	
l. 80 :
Remplacez :
def type2
      if name =="Motisma"
        return 14 if @form == 0
        return 2 if @form == 1
        return 3 if @form == 2
        return 6 if @form == 3
        return 10 if @form == 4
        return 5 if @form == 5
      end #else
      return @type2
    end
	
Par :
def type2
      case name
      when "Motisma"
        list = [14, 2, 3, 6, 10, 5]
        type = list[@form]
      else
        type = @type2
      end
      return type
    end

l. 98 :
Remplacez :
def base_atk
      if name == "Deoxys"
        print 180 if @form == 1
        return 180 if @form == 1
        return 70 if @form == 2
        return 95 if @form == 4
      end
      if name == "Giratina"
        return 120 if @form == 1
      end
      temp_base_atk
    end
	
Par :
def base_atk
      atk_tmp = nil
      case name
      when "Deoxys"
        list = [180, 70, nil, 95]
        atk_tmp = list[@form] if @form < list.size
      when "Giratina"
        atk_tmp 120 if @form == 1
      end
      return atk_tmp if atk_tmp != nil
      temp_base_atk
    end
	
l. 113 :
Remplacez :
def base_dfe
      if name == "Deoxys"
        return 20 if @form == 1
        return 160 if @form == 2
        return 90 if @form == 4
      end
      if name == "Giratina"
        return 100 if @form == 1
      end
      temp_base_dfe
    end
	
Par :
def base_dfe
      dfe_tmp = nil
      case name
      when "Deoxys"
        list = [20, 160, nil, 90]
        dfe_tmp = list[@form] if @form < list.size
      when "Giratina"
        dfe_tmp 100 if @form == 1
      end
      return dfe_tmp if dfe_tmp != nil
      temp_base_dfe
    end
	
l. 127 :
Remplacez :
def base_spd
      if name == "Deoxys"
        return 150 if @form == 1
        return 90 if @form == 2
        return 180 if @form == 4
      end
      temp_base_spd
    end
	
Par :
def base_spd
      spd_tmp = nil
      case name
      when "Deoxys"
        list = [150, 90, nil, 180]
        spd_tmp = list[@form] if @form < list.size
      end
      return spd_tmp if spd_tmp != nil
      temp_base_spd
    end
	
l. 140 :
Remplacez :
def base_ats
      if name == "Deoxys"
        return 180 if @form == 1
        return 70 if @form == 2
        return 95 if @form == 4
      end
      if name == "Giratina"
        return 120 if @form == 1
      end
      temp_base_ats
    end
	
Par :
def base_ats
      ats_tmp = nil
      case name
      when "Deoxys"
        list = [180, 70, nil, 95]
        ats_tmp = list[@form] if @form < list.size
      when "Giratina"
        ats_tmp 120 if @form == 1
      end
      return ats_tmp if ats_tmp != nil
      temp_base_ats
    end
	
l. 153
Remplacez :
def base_dfs
      if name == "Deoxys"
        return 20 if @form == 1
        return 160 if @form == 2
        return 90 if @form == 4
      end
      if name == "Giratina"
        return 100 if @form == 1
      end
      temp_base_dfs
    end
	
Par :
def base_dfs
      dfs_tmp = nil
      case name
      when "Deoxys"
        list = [20, 160, nil, 90]
        dfs_tmp = list[@form] if @form < list.size
      when "Giratina"
        dfs_tmp 100 if @form == 1
      end
      return dfs_tmp if dfs_tmp != nil
      temp_base_dfs
    end

l. 237
Remplacez :
def change_stat(stat_id, amount = 0, actor_change = self)
      # Corps sain / Clear Body (ab) // Mist / Brume
      if amount < 0 and (effect_list.include?(:brume) and 
                         actor_change.ability_symbol != 145 or 
                         @ability == 29 or 
                         effect_list.include?(:defense_spec))
        return 0 
      end
      # Regard Vif / Keen Eye (ab)
      if amount < 0 and stat_id == 6 and @ability == 51
        return 0
      end
      # Hyper Cutter (ab)
      if amount < 0 and stat_id == 0 and @ability == 52
        return 0
      end
      temp_change_stat(stat_id, amount, actor_change)
    end
	
Par :
def change_stat(stat_id, amount = 0, actor_change = self)
      # Corps sain / Clear Body (ab) // Mist / Brume
      if amount < 0 and (effect_list.include?(:brume) and 
                         actor_change.ability_symbol != :infiltration or 
                         ability_symbol == :corps_sain or 
                         effect_list.include?(:defense_spec))
        return 0 
      end
      # Regard Vif / Keen Eye (ab)
      if amount < 0 and stat_id == 6 and ability_symbol == :regard_vif
        return 0
      end
      # Hyper Cutter (ab)
      if amount < 0 and stat_id == 0 and ability_symbol == :hyper_cutter
        return 0
      end
      temp_change_stat(stat_id, amount, actor_change)
    end
	
l. 254 :
Remplacez :
def spd_modifier
      n = 1
      # Swift Swim / Glissade (ab)
      if @ability == 33 and $battle_var.rain?
        n *= 2
      end
      # Chlorophyle (ab)
      if @ability == 34 and $battle_var.sunny?
        n *= 2
      end
      return n*temp_spd_modifier
    end
	
Par :
def spd_modifier
      n = 1
      # Swift Swim / Glissade (ab)
      if ability_symbol == :glissade and $battle_var.rain?
        n *= 2
      end
      # Chlorophyle (ab)
      if ability_symbol == :chlorophyle and $battle_var.sunny?
        n *= 2
      end
      return n*temp_spd_modifier
    end
	
l. 268 :
Remplacez :
def sleep_check
      if @ability == 48 # Matinal / Early Bird (ab)
        @status_count -= 1
      end
      temp_sleep_check
    end
	
Par :
def sleep_check
      if ability_symbol == :matinal
        @status_count -= 1
      end
      temp_sleep_check
    end
	
Dans Pickup_battle l.7, remplacez :
if p.ability == 53 and p.item_hold == 0

Par :
if p.ability_symbol == :ramassage and p.item_hold == 0

+ Correctif Pension propos� par Symbioss :
Dans DAYCARE l. 98, remplacez :
if $game_variables[PENSION][4] == 1

Par :
# Nombre de pas avant l'apparition d'un oeuf
        if $game_variables[PENSION][4] == 128