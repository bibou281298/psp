APRES RESTRUCT V2

dépendance : aucune

Par maror

Dans Pokemon_Battle_Methods_Animation l.84 :
En-dessous de :
elsif not(hit)
        wait(40)
        draw_text(user.given_name, "rate son attaque !")
        wait(40)
      end
	  
Ajoutez :
if hit and user.ability_symbol == :pickpocket and 
         user_skill.direct? and damage > 0 and user.item_hold == 0 and 
         @target.item_hold != 0
        draw_text("#{@actor.given_name} vole l'objet","de #{@target.given_name} !")
        user.item_hold = @target.item_hold
        @target.item_hold = 0
        wait(40)
      end