APRES RESTRUCT V2

d�pendance : aucune

Dans Pokemon_Save, l.354 :
Au-dessus de :
  end
end

class Interpreter
Ajoutez :
# Cr�ation d'une fen�tre de texte
    # Renvoie les d�tails de la fen�tre
    def create_text()
      text_window = Window_Base.new(0, 375, 632, $fontsize * 2 + 34)
      text_window.z = @new_save_window.z + 10
      text_window.contents = Bitmap.new(600, 104)
      text_window.contents.font.name = "Pokemon DP"
      text_window.contents.font.size = $fontsize

      return text_window
    end
      
    # Ecriture sur la fen�tre de texte d'une taille de 2 lignes
    # line1 : La premi�re ligne du block de texte
    # line2 : La deuxi�me ligne du block de texte
    def draw_text(line1 = "", line2 = "")
      @text_window.visible = true
      @text_window.contents.clear
      @text_window.draw_text(0, -8, 460, 50, line1, 0, @text_window.normal_color)
      @text_window.draw_text(0, 22, 460, 50, line2, 0, @text_window.normal_color)
    end
	
l.77 :
Au-dessus de :
loop do
        Graphics.update
        Input.update
        update
        if $scene != self
          break
        end
      end
Ajoutez :
@text_window = create_text
      @text_window.visible = false
	  
l.113 :
En-dessous de :
if @index == @number # Nouvelle sauvegarde
          $game_system.se_play($data_system.save_se)
          filename = "Save#{@number + 1}.rxdata"
          file = File.open(filename, "wb")
          write_save_data(file)
          file.close
          # En cas d'appel d'un �v�nement
          if $game_temp.save_calling
            # Effacer l'indicateur de sauvegarde de l'appel
            $game_temp.save_calling = false
            # Passer � l'�cran de la carte
            $scene = Scene_Map.new
            return
          end
          # Passer � l'�cran de menu
          $scene = POKEMON_S::Pokemon_Menu.new(4)
        else
          decision = draw_choice
          if decision
            $game_system.se_play($data_system.save_se)
            filename = "Save#{@index + 1}.rxdata"
            file = File.open(filename, "wb")
            write_save_data(file)
            file.close
            # En cas d'appel d'un �v�nement
            if $game_temp.save_calling
              # Effacer l'indicateur de sauvegarde de l'appel
              $game_temp.save_calling = false
              # Passer � l'�cran de la carte
              $scene = Scene_Map.new
              return
            end
            # Passer � l'�cran de menu
            $scene = POKEMON_S::Pokemon_Menu.new(4)
          else
            return
          end
        end		
Ajoutez :
draw_text("La sauvegarde a bien �t� effectu�e !")
        loop do
          Graphics.update
          Input.update
          @text_window.update
          break if Input.trigger?(Input::C)
        end
		
l.85 :
Au-dessus de :
@new_save_window.dispose
      @background.dispose
Ajoutez :
@text_window.dispose