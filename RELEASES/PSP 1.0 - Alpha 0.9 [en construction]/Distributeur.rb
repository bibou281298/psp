APRES RESTRUCT V2

dépendance : aucune

Supprimez les scripts :
Pokemon_Shop / Pokemon_Shop_Buy / Pokemon_Shop_List

Installez les scripts dans le dossier "Distributeur" :
Pokemon_Shop.rb / Pokemon_Shop_Buy.rb / Pokemon_Shop_Buy_Distrib.rb / Pokemon_Shop_Distrib.rb / Pokemon_Shop_List.rb / Pokemon_Shop_List_Distrib.rb

Dans Interpreter Bis :
Remplacez :
$scene = Pokemon_Shop.new(shop_list) 
Par :
if $game_switches[DISTRIBUTEUR]
  $scene = Pokemon_Shop_Distrib.new(shop_list)
else
  $scene = Pokemon_Shop.new(shop_list)
end

Dans Gestion_Switches_Variables :
Ajoutez dans la section interrupteurs : DISTRIBUTEUR = 12