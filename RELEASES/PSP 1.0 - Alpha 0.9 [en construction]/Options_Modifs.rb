APRES RESTRUCT V2

Dépendances : Menu Options

Petit tips pour la suite, Je vous demanderai à plusieurs reprises
de remplacer tous les ..., Pour cela, rendez-vous dans le script
indiqué et faites CTRL + H. Dans la première boîte, entrez la
ligne à remplacer et dans la seconde ce qui va la remplacer
puis appuyez sur Remplacer Tout

Dans Scene_Map (Système Général), Remplacez tous les :
"Fenêtre"
par des
0
Puis remplacez tous les : 
"Plein Écran"
par des 
1

Dans TOUS les scripts liés au Battle Core, remplacez tous les
"Désactiver"
par des :
0
Puis remplacez tous les :
"Activer"
par des :
1
Ensuite, remplacez tous les :
"Choix"
par des :
0
Puis tous les :
"Défini"
par des :
1

Dans Config Panel,
l(46)Remplacez :
MSG = "messagedummy.png"
par :
MSG = "messagedummy_0.png"

#Dans l'onglet Système de la BDD, remplacez ce qu'il y a dans
#Fenêtre par le premier windowskin dans BOX_LIST du système
#d'Options

Les variables pour les boîtes de dialogue et pour les polices ont été déplacés
en haut du script du Menu Options. Si vous avez modifié ces listes,
vous devrez les modifier de nouveau.

#La suite n'est pas obligatoire, mais je vous conseille de la
#suivre pour une meilleure optimisation de l'écriture des scripts
#ça n'impactera rien en jeu

Dans Game_System

l(46)Remplacez
if $bgm_master == nil
par
unless $bgm_master

l(89)Remplacez
if $bgs_master == nil
par
unless $bgs_master

l(125)Remplacez
if $me_master == nil
par
unless $me_master

l(141)Remplacez
if $se_master == nil
par
unless $se_master

Dans Window_Message (Système Général) 

l(34) Remplacez :
if $vit_txt == nil
Par :
unless $vit_txt

l(23) Remplacez :
if $message_dummy == nil
par :
unless $message_dummy

Dans Pokemon_Window_Help

l(25)Remplacez :
if $message_dummy == nil
par :
unless $message_dummy

Dans Pokemon_Shop

l(39)Remplacez :
if $message_dummy == nil
par :
unless $message_dummy

Dans Window_Message de Messages et Multichoix, 

Remplacez tous les:
if $vit_txt == nil
par des : 
unless $vit_txt

l(102)if $message_dummy == nil
par :
unless $message_dummy

Dans FModEx
Les lignes sont très différentes sur mon projet, je vous laisse
faire des CTRL + F

Remplacez
if $bgm_master == nil
par
unless $bgm_master

Remplacez
if $bgs_master == nil
par
unless $bgs_master

Remplacez
if $me_master == nil
par
unless $me_master

Remplacez
if $se_master == nil
par
unless $se_master