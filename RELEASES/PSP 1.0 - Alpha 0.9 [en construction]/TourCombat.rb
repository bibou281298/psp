APRES RESTRUCT V2

d�pendance : aucune

Dans Pokemon_Methods_Level l.371 :
Au-dessus de :
#------------------------------------------------------------    
    # Exp�rience � acqu�rir pour le prochain niveau
    #------------------------------------------------------------ 
    def next_exp
      if @level >= MAX_LEVEL
        return 0
      end
      return (exp_list[@level+1]-@exp)
    end
Ajoutez :
# Modification forc�e du niveau
    # level : Level attribu�
    def refresh_level(new_level)
      @level_save = @level
      @level = new_level
      @hp = maxhp_basis
      @atk = atk_basis
      @dfe = dfe_basis
      @spd = spd_basis
      @ats = ats_basis
      @dfs = dfs_basis
    end
	
Dans Pokemon l.62 :
En-dessous de :
attr_accessor :level
Ajoutez :
attr_accessor :level_save

Dans Pokemon_Battle_Core l.50 :
En-dessous de :
def main
Ajoutez :
# Si la variable est d�finie, alors force le niveau d�finit dans la
      # variable � l'acteur et � l'ennemi
      if $game_variables[LEVEL_TOUR_COMBAT] > 0
        refresh_level
      end
	  
Dans Pokemon_Battle_End l.36 :
En-dessous de :
def end_battle(result = 0)
      pickup if result == 0
Ajoutez :
# Si la variable est d�finie, alors doit r�tablir le niveau initial
      # avant le d�but du combat
      if $game_variables[LEVEL_TOUR_COMBAT] > 0
        refresh_level(true)
      end
	  
Dans Pokemon_Battle_Methods 1 l.204 :
En-dessous de : 
#------------------------------------------------------------  
    # Reset party
    #------------------------------------------------------------
    # R�initialise l'ensemble des acteurs d'une partie (utile en fin de combat)
    # party : le type de partie o� les acteurs appartenant � cette derni�re
    # ont besoin d'�tre r�initialis�
    def reset_party(party)
      party.actors.each { |actor| actor.reset_stat_stage }
    end
	
Ajoutez :
#------------------------------------------------------------  
    # Force le level des �quipes
    #------------------------------------------------------------
    # Donne un niveau d�finit � tous les pok�mon qui peuvent �tre mis en
    # combat
    # fin_combat : si true r�tablit le level initial
    def refresh_level(fin_combat = false)
      level = $game_variables[LEVEL_TOUR_COMBAT]
      equipes = [@party.actors, $battle_var.enemy_party.actors]
      equipes.each do |party|
        party.each do |pokemon|
          level = pokemon.level_save if fin_combat # Niveau initial du pok�mon
          pokemon.refresh_level(level)
        end
      end
    end
	  
Dans Pokemon_Battle_Trainer :
Remplacez les :
exp_battle 
Par :
exp_battle if not $game_switches[EXP_BLOQUE]

l.348 :
Remplacez :
if alive > 1 and $battle_style != "D�fini"
Par :
if alive > 1 and $battle_style != "D�fini" and
           not $game_switches[SWITCH_POKEMON_BLOQUE]
		   
l.657 :
Remplacez :
@evolve_checklist.each do |actor|
        info = actor.evolve_check
        if info != false
          scene = Pokemon_Evolve.new(actor, info, @z_level + 200)
          scene.main
        end
      end
	  
Par :
if @evolve_checklist != nil
        @evolve_checklist.each do |actor|
          info = actor.evolve_check
          if info != false
            scene = Pokemon_Evolve.new(actor, info, @z_level + 200)
            scene.main
          end
        end
      end

Dans Pokemon_Battle_Wild l.196 :
Remplacez :
else
          draw_text("Voulez-vous utiliser", "un autre Pok�mon?")
Par :
elsif not $game_switches[SWITCH_POKEMON_BLOQUE]
          draw_text("Voulez-vous utiliser", "un autre Pok�mon?")

l.561 :
Remplacez :
1.upto(amount) do |i|
            actor.add_exp_battle(1)
            if actor.level_check
              actor.level_up(self)
              evolve_checklist.push(actor)
            end
            if actor == @actor
              if @actor.exp_list[@actor.level+1] != nil and @actor.level < MAX_LEVEL
                divide = (@actor.exp_list[@actor.level+1]-@actor.exp_list[@actor.level])/192
                if divide == 0
                  divide = 1
                end
                if (@actor.exp - @actor.exp_list[@actor.level])%divide == 0
                  @actor_status.exp_refresh
                  $scene.battler_anim ; Graphics.update
                end
              end
            end
          end
Par :
if not $game_switches[EXP_BLOQUE]
            1.upto(amount) do |i|
              actor.add_exp_battle(1)
              if actor.level_check
                actor.level_up(self)
                evolve_checklist.push(actor)
              end
              if actor == @actor
                if @actor.exp_list[@actor.level+1] != nil and @actor.level < MAX_LEVEL
                  divide = (@actor.exp_list[@actor.level+1]-@actor.exp_list[@actor.level])/192
                  if divide == 0
                    divide = 1
                  end
                  if (@actor.exp - @actor.exp_list[@actor.level])%divide == 0
                    @actor_status.exp_refresh
                    $scene.battler_anim ; Graphics.update
                  end
                end
              end
            end
          end
		  
Dans Gestion_Switches_Variables :
Parmi les interrupteurs, ajoutez : 
EXP_BLOQUE = 85 # Si true alors les acteurs ne gagnent pas de points d'exp�riences
SWITCH_POKEMON_BLOQUE = 86 # Si true alors le choix de switch n'est pas affich�
                           # lorsque le pok�mon adverse est K.O.
Parmi les variables, ajoutez :
LEVEL_TOUR_COMBAT = 36 # Variables d�finissant le niveau des pok�mon pendant un combat
                       # Si 0 alors les niveaux ne sont pas retouch�s